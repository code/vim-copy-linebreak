"
" copy_linebreak.vim: Bind user-defined key sequences to toggle a group of
" options that make text wrapped with 'wrap' copy-paste friendly.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_copy_linebreak') || &compatible || v:version < 700
  finish
endif
let loaded_copy_linebreak = 1

" Provide mappings to the functions just defined
nnoremap <silent> <unique>
      \ <Plug>(CopyLinebreakEnable)
      \ :<C-U>call copy_linebreak#Enable()<CR>
nnoremap <silent> <unique>
      \ <Plug>(CopyLinebreakDisable)
      \ :<C-U>call copy_linebreak#Disable()<CR>
nnoremap <silent> <unique>
      \ <Plug>(CopyLinebreakToggle)
      \ :<C-U>call copy_linebreak#Toggle()<CR>
