" Window-local options to set back to Vim defaults while copy-friendly
" line-breaking options are in effect; note we have to switch on the presence
" of patch 8.1.2281 to find a workable value for 'showbreak'.
"
" <https://github.com/vim/vim/releases/tag/v8.1.2281>
"
let s:options = {
      \  'breakindent': 0,
      \  'colorcolumn': '',
      \  'cursorcolumn': 0,
      \  'cursorline': 0,
      \  'linebreak': 0,
      \  'showbreak':
      \    has('patch-8.1.2281') ? 'NONE' : '',
      \}

" Filter that list for only the options that actually exist in this version of
" Vim; we use & rather than +, because it's OK if the option doesn't work, we
" just don't want to throw errors when we try to get/set it.
"
call filter(s:options, 'exists(''&''.v:key)')

" Helper function to set a local option with :execute
function! s:SetOption(option, value) abort
    execute 'let &l:'.a:option.' = a:value'
endfunction

" Enable copy-friendly 'linebreak' options
function! copy_linebreak#Enable() abort

  " Set appropriate window-local options to their Vim defaults, saving their
  " values into a window-scoped dictionary first, so that #Disable() can put
  " them back when we're done.
  "
  let w:copy_linebreak_save = {}
  for option in keys(s:options)
    let w:copy_linebreak_save[option] = eval('&l:'.option)
    call s:SetOption(option, s:options[option])
  endfor

  " Report the new value of 'linebreak' to the user
  setlocal linebreak?

endfunction

" Disable copy-friendly 'linebreak' options
function! copy_linebreak#Disable() abort

  " Set appropriate window-local options back to the values to which they were
  " set before #Enable() messed with them, if a window-local dictionary to do
  " such exists.
  "
  if exists('w:copy_linebreak_save')
    for option in keys(w:copy_linebreak_save)
      call s:SetOption(option, w:copy_linebreak_save[option])
    endfor
    unlet w:copy_linebreak_save
  endif

  " Report the new value of 'linebreak' to the user
  setlocal linebreak?

endfunction

" Toggle copy-friendly linebreak options, using the current setting for the
" 'linebreak' option as the pivot.
"
function! copy_linebreak#Toggle() abort
  if &linebreak
    call copy_linebreak#Enable()
  else
    call copy_linebreak#Disable()
  endif
endfunction
