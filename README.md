copy\_linebreak.vim
===================

This plugin provides mapping targets for a user to set, unset, or toggle
`'linebreak'`-related settings when `'wrap'` is enabled, to switch between
human-readable output and a format friendly for copy-pasting with terminal
emulators or screen/tmux.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
